// 引入UserService模块，该模块提供了与数据库中的用户数据交互的方法  
const UserService = require("../../services/admin/UserService");

// 引入JWT模块，该模块用于生成和验证JSON Web Tokens  
const JWT = require("../../util/JWT");

// 定义UserController对象，它包含了一个登录方法  
const UserController = {
  // 登录方法，它是异步的，接收请求对象(req)和响应对象(res)作为参数  
  login: async (req, res) => {
    // 打印请求体中的数据，用于调试  
    console.log(req.body);

    // 使用UserService的login方法查询用户数据，将请求体中的数据作为参数传入  
    // 如果UserService返回的数据长度为0，则表示没有找到匹配的数据  
    var result = await UserService.login(req.body);
    console.log(result.length);
    // 判断查询结果是否为空  
    if (result.length === 0) {
      // 如果查询结果为空，说明用户名和密码不匹配，向客户端返回错误信息  
      res.send({
        code: "-1",
        error: "用户名密码不匹配",
      });
    } else {
      // 如果查询结果不为空，说明用户名和密码匹配，生成token并返回给客户端  
      // 生成token的方法是：将用户ID和用户名作为payload，并使用"1d"作为密钥进行签名，生成一个token  
      const token = JWT.generate({
        _id: result[0]._id,
        username: result[0].username
      }, "1d")

      // 将生成的token添加到响应头中，客户端可以通过这个token进行后续的请求验证  
      res.header("Authorization", token)

      // 向客户端返回成功信息和用户数据  
      res.send({
        ActionType: "OK",
        data: {
          username: result[0].username,  // 用户名  
          gender: result[0].gender ? result[0].gender : 2,  // 性别，如果没有性别信息则默认为0：保密  
          introduction: result[0].introduction,  // 个人简介  
          avatar: result[0].avatar,  // 头像URL  
          role: result[0].role,  // 角色，管理员为1，编辑为2  
        }
      });
    }
  },

  //更新个人信息
  upload: async (req, res) => {
    // console.log(req.body, req.file);
    const { username, introduction, gender } = req.body
    const token = req.headers["authorization"].split(" ")[1];
    const avatar = req.file ? `/avataruploads/${req.file.filename}` : ""
    var payload = JWT.verify(token);
    // console.log(payload._id);
    //调用service模块更新数据
    await UserService.upload({
      _id: payload._id,
      username, introduction, gender: Number(gender), avatar
    })
    if (avatar) {  ////判断是否上传图像 没有上传的话就不更新
      res.send({
        ActionType: "OK",
        data: {
          username, introduction, gender: Number(gender), avatar
        }
      })
    } else {
      res.send({
        ActionType: "OK",
        data: {
          username, introduction, gender: Number(gender)
        }
      })
    }
  },

  //新增人员
  add: async (req, res) => {
    // console.log(req.body, req.file);
    const { username, introduction, gender, role, password } = req.body
    const avatar = req.file ? `/avataruploads/${req.file.filename}` : ""
    //调用service模块更新数据
    await UserService.add({
      username, password, introduction, gender: Number(gender), avatar, role: Number(role)
    })
    res.send({
      ActionType: "OK",
    })
  },

  getList: async (req, res) => {
    const result = await UserService.getList(req.params)
    res.send({
      ActionType: "OK",
      data: result
    })
  },

  putList:async(req,res)=>{
    const result = await UserService.putList(req.body)
    res.send({
      ActionType: "OK",
    })
  },

  delList:async(req, res)=>{
    //console.log(req.params.id);

    const result = await UserService.delList({_id:req.params.id})
    res.send({
      ActionType: "OK",
      data: result
    })
  },


};

// 将UserController对象导出，以便在其他文件中使用  
module.exports = UserController;