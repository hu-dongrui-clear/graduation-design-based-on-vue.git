// 引入 mongoose 模块，它是与 MongoDB 数据库进行交互的 Node.js 库  
const mongoose = require("mongoose")  
  
// 从 mongoose 引入 Schema 对象，它是用于定义 MongoDB 文档结构的类  
const Schema = mongoose.Schema  
  
const ProductType = {  
    title:String,  //标题
    introduction: String,//简要描述
    detail: String,//详细描述
    cover: String,//封面图片
    editTime:Date
}  
  
const ProductModel = mongoose.model("product", new Schema(ProductType)) 
  
module.exports = ProductModel 