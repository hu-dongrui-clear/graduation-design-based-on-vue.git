const ProductModel = require("../../models/ProductModel")

const ProductService = {
    add:async({title, introduction, detail,cover,editTime})=>{
        //console.log("数据库模型处理");
        return ProductModel.create({
            title, introduction, detail,cover,editTime
        })
    },
    updateList:async({_id,title, introduction, detail,cover,editTime})=>{
            if (cover) {
                return ProductModel.updateOne({  _id   },{
                    _id,title, introduction, detail,cover,editTime
                })
            }else{
                return ProductModel.updateOne({  _id   },{
                    _id,title, introduction, detail,editTime
                })
            }
   
    },
    getList:async({_id})=>{
        return _id? ProductModel.find({_id}):ProductModel.find({})//先根据_id找，如果没有id就找全部数据
    },

    delList:async({_id})=>{
        return ProductModel.deleteOne({
            _id
        })
    },
}

module.exports = ProductService