# 使用说明

## 一、数据库的使用

### **1.下载MongoDB离线版用于启动MongoDB数据库** 

   [Download MongoDB Community Server | MongoDB](https://www.mongodb.com/try/download/community)  

![image-20231104172002399](img/image-20231104172002399.png)

### 2.要下载zip文件，解压后

![image-20231104172105679](img/image-20231104172105679.png)

### 3.在bin目录下启动  

mongod --dbpath=D:\VSC\VscodWork\graduation-design-based-on-vue\db

D:\VSC\VscodWork\graduation-design-based-on-vue\db　：此处这路径要换成自己电脑上项目的路径

回车后：

![image-20231104172304565](img/image-20231104172304565.png)

### 4.启动成功后使用可视化工具

MongoDBCompass　进行查看数据库　下载地址：[MongoDB Compass | MongoDB](https://www.mongodb.com/products/tools/compass)

直接默认安装即可。

## 二、服务器的启动

在进入server服务器目录下进行**npm i**进行安装node_modules

然后再npm start进行启动服务器

**！！！注意！！！**在运行的电脑上一定是已经安装过node.js,并且已经配置过环境变量的，否则将无法运行。

如果报nodemon相关的错误 在server服务器下 npm install -g nodemon  全局安装nodemon
nodemon 是一款外置模块用于代码更改时重启服务器，免去每次更改代码都需要重新启动的麻烦事

![image-20231104172949426](img/image-20231104172949426.png)

**在服务器启动后，此时服务器已经通过命令创建了相应的数据库，以及数据表，但由于项目打包时将数据库资料是分开的，此时我们需要打开MongoDB compass可视化工具，进行数据资源导入，方可进行登录。**

**进入MongoDB compass后直接点connect进行连接数据库。**

![image-20231104173537979](img/image-20231104173537979.png)

之后便可以看见服务器操作mongodb代码创建的数据库，以及数据表，

![image-20231104173922152](img/image-20231104173922152.png)

此时数据表是没有资源的，此时我们可以将事先准备好的数据表进行导入

![image-20231104174203726](img/image-20231104174203726.png)

全部默认选项，直接点击import

![image-20231104174227425](img/image-20231104174227425.png)

导入成功

![image-20231104174236530](img/image-20231104174236530.png)

同理导入users表和news表即可。

### **登录账户：胡董瑞**

### **登录密码：123456**

## 三、Vue的启动

同理在启动Vue项目时也需要npm i

![image-20231104174648615](img/image-20231104174648615.png)

![image-20231104174559525](img/image-20231104174559525.png)

![image-20231104174626255](img/image-20231104174626255.png)

项目的所有准备工作已经完成
