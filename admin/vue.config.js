const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer:{
    proxy:{//反向代理
      "/adminapi":{
        target:"http://localhost:3000",
        changeOrigin:true
      }
    }
  }
})
