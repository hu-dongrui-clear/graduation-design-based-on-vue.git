import { createRouter, createWebHashHistory } from "vue-router";
import Login from "../views/Login.vue";
import MainBox from "../views/MainBox.vue"; //大的首页（登录进去后的界面）
import RoutesConfig from "./config";
import store from "../store/index";
const routes = [
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/mainbox",
    name: "mainbox",
    component: MainBox,
    //MainBox的子路由 后面根据权限动态添加
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

//路由守卫 每次路由跳转之前都会执行这个函数
router.beforeEach((to, from, next) => {
  if (to.name === "login") {    //第一次如果是login就放行
    next(); //next()函数被调用才能跳转路由
  } else {
    if (!localStorage.getItem("token")) {      //判断本地有没有token值
      next({        //如果未授权重定向到login
        path: "/login", //没有token跳到login
      });
    } else {
      //如果已授权（已经登录过）
      if (!store.state.isGetterRouter) {

        router.removeRoute("mainbox")
        ConfigRouter();
        next({
          path: to.fullPath
        });
      } else {
        next();
      }
    }
  }
});

const ConfigRouter = () => {
  RoutesConfig.forEach((item) => {
    if (!router.hasRoute("mainbox")) {
      router.addRoute({
        path: "/mainbox",
        name: "mainbox",
        component: MainBox,
      })
    }
    //遍历RoutesConfig数组给mainbox添加子路由
    checkPermission(item) && router.addRoute("mainbox", item);
  });
  //改变isGetterRouter = true
  store.commit("changeGetterRouter", true);
};

const checkPermission = (item) => {
  if (item.requireAdmin) {
    return store.state.userInfo.role === 1
  }
  return true
}

export default router;
